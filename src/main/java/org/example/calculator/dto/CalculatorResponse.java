package org.example.calculator.dto;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class CalculatorResponse {

    private int result;
}
