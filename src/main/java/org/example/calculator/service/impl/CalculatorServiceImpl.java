package org.example.calculator.service.impl;

import org.example.calculator.service.CalculationService;
import org.springframework.stereotype.Service;

@Service
public class CalculatorServiceImpl implements CalculationService {


    @Override
    public int toplama(int a, int b) {
        return a+b;
    }



    @Override
    public int cixma(int a, int b) {
        return a-b;
    }

    @Override
    public int vurma(int a, int b) {
        return a*b;
    }

    @Override
    public int bolme(int a, int b) {
        return a/b;
    }
}
