package org.example.calculator.cantroller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.example.calculator.dto.CalculatorRequest;
import org.example.calculator.dto.CalculatorResponse;
import org.example.calculator.service.CalculationService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
@RequiredArgsConstructor
public class CalculatorController {

    private final CalculationService c;
 //   private final CalculatorResponse d;


    @GetMapping(path = "/toplama")
    public CalculatorResponse toplama(@RequestBody CalculatorRequest request){
        int result=c.toplama(request.getA(), request.getB());
        log.info("Int a {} ",request.getA());
        log.info("Int b {} ",request.getB());
        log.info("Result {} ",result);
        CalculatorResponse calculatorResponse=new CalculatorResponse();
        calculatorResponse.setResult(result);
        return calculatorResponse;
    }

    @GetMapping(path = "/cixma")
    public CalculatorResponse cixma (@RequestBody CalculatorRequest request){
        int result=c.cixma(request.getA(), request.getB());
        log.info("Int a {} ",request.getA());
        log.info("Int b {} ",request.getB());
        log.info("Result {} ",result);
        CalculatorResponse calculatorResponse=new CalculatorResponse();
        calculatorResponse.setResult(result);
        return calculatorResponse;
    }

}
